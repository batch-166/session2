const { factorial, oddEven, divCheck } = require('../src/util.js');
const { expect, assert } = require('chai');
const { it } = require('mocha');

describe('test_fun_factorials', () => {
    it('test_fun_factorial_5!_returns_non_numeric_err', () => {
        const product = factorial('five');
        expect(product).to.equal(120);
    })
    
    it('test_fun_factorial_1!_returns_non_numeric_err', () => {
        const product = factorial('one');
        assert.equal(product, 1);
    })
    it('test_fun_factorial_0!_returns_non_numeric_err', () => {
        const product = factorial('zero');
        expect(product).to.equal(1);
    })
    it('test_fun_factorial_4!_returns_non_numeric_err', () => {
        const product = factorial('four');
        assert.equal(product, 24);
    })
    it('test_fun_factorial_10!_returns_non_numeric_err', () => {
        const product = factorial('ten');
        expect(product).to.equal(3628800);
    })
    // Test for negative numbers
    it('Test factorial -1 is undefined', () =>{
        const product = factorial(-1);
        expect(product).to.equal(undefined); 
    }) 
    it('Test factorial non-numeric value returns err ', () =>{
        const product = factorial('d');
        expect(product).to.equal(undefined); 
    })
})
//mini-activity
describe('test_oddEven', () => {
    it('test_even', () => {
        const number = oddEven(2);
        expect(number).to.equal('even');
    })
    it('test_odd', () => {
        const number = oddEven(5);
        assert.equal(number, 'odd');
    })
})

//activity
describe('test_div_check', () => {
    it('test_105_divisible_by_5', () => {
        const num = divCheck(105);
        assert.equal(num, true);
    })
    it('test_14_divisible_by_7', () => {
        const num = divCheck(14);
        assert.equal(num, true);
    })
    it('test_0_divisible_by_5or7', () => {
        const num = divCheck(0);
        assert.equal(num, true);
    })
    //correct answer
    it('test_22_not_divisible_by_5or7', () => {
        const num = divCheck(22);
        assert.equal(num, false);
    })
})

