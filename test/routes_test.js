const chai = require('chai');

// same as const {expect} = require('chai')
const expect = chai.expect;

const http = require('chai-http');
chai.use(http);

describe('API Test Suite', () => {
    it('Test API GET People are running', () => {
        chai.request('http://localhost:5001').get('/people')
            .end((err, res) => {
                expect(res).to.not.equal(undefined);
            })
    })
    it('Test API GET people returns 200', (done) => {
        chai.request('http://localhost:5001')
            .get('/people')
            .end((err, res) => {
                expect(res.status).to.equal(200);
                done();
            })
    })
    it('Test API POST Person returns 400 if no person name', (done) => {
        chai.request('http://localhost:5001')
            .post('/person')
            .type('json')
            .send({
                alias: 'Jason',
                age: 28
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
            })
    })
    it('Check if Person endpoint is running', () => {
        chai.request('http://localhost:5001')
        .post('/person')
        .type('json')
        .send({
            alias: 'Shan',
            name: 'Valentine',
            age: 45
        })
            .end((err, res) => {
                expect(res).to.not.equal(undefined);
            })
    })
    it('Test API POST Person returns 400 if no ALIAS', (done) => {
        chai.request('http://localhost:5001')
            .post('/person')
            .type('json')
            .send({
                relationship: 'engaged',
                age: 28
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
            })
    })
    it('Test API POST Person returns 400 if no AGE', (done) => {
        chai.request('http://localhost:5001')
            .post('/person')
            .type('json')
            .send({
                alias: 'Cameron',
                job : 'Construction Head'
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
            })
    })
})